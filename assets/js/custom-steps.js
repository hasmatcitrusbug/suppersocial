// $("#the-basic-step1 > .step-cover > .cover-thumb").addClass("view-step" + parseFloat(100 / $("#the-basics-step-1").data('steps'))+'%');

  var title_category1 = $('#title_category1');
  var title_category1_Validator = title_category1.validate();

  var title_category2 = $('#title_category2');
  var title_category2_Validator = title_category2.validate();

  var details_validation3 = $('#details_validation3');
  var details_validation3_Validator = details_validation3.validate();
  
$('#step-wizard-basic').steps({
  
    onChange: function (currentIndex, newIndex, stepDirection) {
      // step1
      if (currentIndex === 0) {
        if (stepDirection === 'forward') {
          return title_category1.valid();          
        }
        if (stepDirection === 'backward') {
          title_category1_Validator.resetForm();
          
        }
        
      }
      if (currentIndex === 1) {
        if (stepDirection === 'forward') {
          return title_category2.valid(); 
        }
        if (stepDirection === 'backward') {
          title_category2_Validator.resetForm();
        }
        
      }
      // step3
      if (currentIndex === 2) {
        if (stepDirection === 'forward') {
          return details_validation3.valid();
        }
        if (stepDirection === 'backward') {
          details_validation3_Validator.resetForm();
        }
      }
      return true;
    },
    onFinish: function () {
      $(".progress-bar-animated").css('width', parseFloat(100 / $("#the-dietary-step2").data('steps'))+'%' );
      $(".step-cover").addClass('step-2');

      $("#the-basics-step-1").addClass("not-visible");
      $("#dietary-step-2").addClass("visible"),$("#dietary-step-2").removeClass("not-visible");
      $("#review-and-post-step-3").addClass("not-visible");
      $("#step-progress-complete").addClass("not-visible");
    }
      
  });
  $('#step-wizard-dietary').steps({
    onFinish: function () {
      $("#the-basics-step-1").addClass("not-visible");
      $("#dietary-step-2").addClass("not-visible"), $("#dietary-step-2").removeClass("visible");
      $("#review-and-post-step-3").addClass("visible").removeClass("not-visible");
      $("#step-progress-complete").addClass("not-visible");
    },
  });

  $("#finish-goto-complete-section").click(function(){
    $("#the-basics-step-1").addClass("not-visible").removeClass("visible");
    $("#dietary-step-2").addClass("not-visible").removeClass("visible");
    $("#review-and-post-step-3").addClass("not-visible").removeClass("visible");
    $("#step-progress-complete").addClass("visible").removeClass("not-visible");
  });

  $("#save-and-close, #save-and-close2").click(function(){
    $("#the-basics-step-1").addClass("not-visible").removeClass("visible");
    $("#dietary-step-2").addClass("not-visible").removeClass("visible");
    $("#review-and-post-step-3").addClass("not-visible").removeClass("visible");
    $("#step-progress-complete").addClass("visible").removeClass("not-visible");
  });

  $("#back-to-edit").click(function(){
    $("#the-basics-step-1").addClass("not-visible").removeClass("visible");
    $("#dietary-step-2").addClass("not-visible").removeClass("visible");
    $("#review-and-post-step-3").addClass("not-visible").removeClass("visible");
    $("#step-progress-complete").addClass("visible").removeClass("not-visible");
  });
   
  $("#add-basics-details").click(function(){
    $("#the-basics-step-1").addClass("visible").removeClass("not-visible");
    $("#dietary-step-2").addClass("not-visible").removeClass("visible");
    $("#review-and-post-step-3").addClass("not-visible").removeClass("visible");
    $("#step-progress-complete").addClass("not-visible").removeClass("visible");
  });

  $(".back1").click(function() {
    if ($(".btn-prev-1").css('display') == 'inline-block') {
      $(".btn-prev-1").click(); 
    }
    else{
      $("#the-basics-step-1").addClass("not-visible").removeClass("visible");
      $("#dietary-step-2").addClass("not-visible").removeClass("visible");
      $("#review-and-post-step-3").addClass("not-visible").removeClass("visible");
      $("#step-progress-complete").addClass("visible").removeClass("not-visible");
    }
  });

  $(".back2").click(function() {
    if ($(".btn-prev-2").css('display') == 'inline-block') {
      $(".btn-prev-2").click(); 
    }
    else{
      $("#the-basics-step-1").addClass("visible").removeClass("not-visible");
      $("#dietary-step-2").addClass("not-visible").removeClass("visible");
      $("#review-and-post-step-3").addClass("not-visible").removeClass("visible");
      $("#step-progress-complete").addClass("not-visible").removeClass("visible");
    }
  });

  
  $(".option-group[name = 'category'] .select.option").on("click", function() {
    var selectedItem = $(this);
    $(".option-group[name = 'category'] .select.option").each(function(index, val) {
      $(this).removeClass('selected');
    });
    selectedItem.addClass('selected');
  });

  $(".option-group[name = 'listing_shape'] .select.option").on("click", function() {
    var selectedItem = $(this);
    $(".option-group[name = 'listing_shape'] .select.option").each(function(index, val) {
      $(this).removeClass('selected');
    });
    selectedItem.addClass('selected');
  });

  
