/* increment & decrement */
  var incrementPlus;
  var incrementMinus;

  var buttonPlus  = $("#total-plus1");
  var buttonMinus = $("#total-minus1");

  var incrementPlus = buttonPlus.click(function() {
    var $n = $(this)
      .parent("#button-container1")
      .parent("#number-group1")
      .find(".qty1");
      $n.val(Number($n.val())+1 );
  });

  var incrementMinus = buttonMinus.click(function() {
      var $n = $(this)
      .parent("#button-container1")
      .parent("#number-group1")
      .find(".qty1");
    var amount = Number($n.val());
    if (amount > 0) {
      $n.val(amount-1);
    }
  });

/* End of increment & decrement */


/* increment & decrement 2 */
var incrementPlus2;
var incrementMinus2;

var buttonPlus2  = $("#total-plus2");
var buttonMinus2 = $("#total-minus2");

var incrementPlus2 = buttonPlus2.click(function() {
  var $n = $(this)
    .parent("#button-container2")
    .parent("#number-group2")
    .find(".qty2");
    $n.val(Number($n.val())+1 );
});

var incrementMinus2 = buttonMinus2.click(function() {
    var $n = $(this)
    .parent("#button-container2")
    .parent("#number-group2")
    .find(".qty2");
  var amount = Number($n.val());
  if (amount > 0) {
    $n.val(amount-1);
  }
});

/* End of increment & decrement 2 */

/* increment & decrement 3 */
var incrementPlus3;
var incrementMinus3;

var buttonPlus3  = $("#total-plus3");
var buttonMinus3 = $("#total-minus3");

var incrementPlus3 = buttonPlus3.click(function() {
  var $n = $(this)
    .parent("#button-container3")
    .parent("#number-group3")
    .find(".qty3");
    $n.val(Number($n.val())+1 );
});

var incrementMinus3 = buttonMinus3.click(function() {
    var $n = $(this)
    .parent("#button-container3")
    .parent("#number-group3")
    .find(".qty3");
  var amount = Number($n.val());
  if (amount > 0) {
    $n.val(amount-1);
  }
});

/* End of increment & decrement 3 */

/* increment & decrement 4 */
var incrementPlus4;
var incrementMinus4;

var buttonPlus4  = $("#total-plus4");
var buttonMinus4 = $("#total-minus4");

var incrementPlus4 = buttonPlus4.click(function() {
  var $n = $(this)
    .parent("#button-container4")
    .parent("#number-group4")
    .find(".qty4");
    $n.val(Number($n.val())+1 );
});

var incrementMinus4 = buttonMinus4.click(function() {
    var $n = $(this)
    .parent("#button-container4")
    .parent("#number-group4")
    .find(".qty4");
  var amount = Number($n.val());
  if (amount > 0) {
    $n.val(amount-1);
  }
});

/* End of increment & decrement 4 */

/* increment & decrement 5 */
var incrementPlus5;
var incrementMinus5;

var buttonPlus5  = $("#total-plus5");
var buttonMinus5 = $("#total-minus5");

var incrementPlus5 = buttonPlus5.click(function() {
  var $n = $(this)
    .parent("#button-container5")
    .parent("#number-group5")
    .find(".qty5");
    $n.val(Number($n.val())+1 );
});

var incrementMinus5 = buttonMinus5.click(function() {
    var $n = $(this)
    .parent("#button-container5")
    .parent("#number-group5")
    .find(".qty5");
  var amount = Number($n.val());
  if (amount > 0) {
    $n.val(amount-1);
  }
});

/* End of increment & decrement 5 */

/* increment & decrement 6 */
var incrementPlus6;
var incrementMinus6;

var buttonPlus6  = $("#total-plus6");
var buttonMinus6 = $("#total-minus6");

var incrementPlus6 = buttonPlus6.click(function() {
  var $n = $(this)
    .parent("#button-container6")
    .parent("#number-group6")
    .find(".qty6");
    $n.val(Number($n.val())+1 );
});

var incrementMinus6 = buttonMinus6.click(function() {
    var $n = $(this)
    .parent("#button-container6")
    .parent("#number-group6")
    .find(".qty6");
  var amount = Number($n.val());
  if (amount > 0) {
    $n.val(amount-1);
  }
});

/* End of increment & decrement 6 */