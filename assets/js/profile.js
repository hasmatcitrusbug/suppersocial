$(document).ready(function(){ 

    /* select 2 */
    $(".js-select2").select2({ minimumResultsForSearch: -1 });

    $("#gender-select").select2({ placeholder: "Select gender" });
    $('#gender-select + .select2').click(function() { $(".select2-container").addClass("active-select2-dropdown");  });

    $("#relationship-status-select").select2({ placeholder: "Select relationship status" });
    $('#relationship-status-select + .select2').click(function() { $(".select2-container").addClass("active-select2-dropdown");  });

    /* end fo select 2 */

    /* datepicker get */
    var FromEndDate = new Date();
    $('#date-picker-dob01').datepicker({ 
        showOtherMonths: true,
    });
    /* End of datepicker get */

    /* profile pic */
    
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".file-upload").on('change', function(){ readURL(this); });
    $(".upload-button").on('click', function() { $(".file-upload").click(); });
    
    /* end of pic */

    /* Add & edit form */

    $("#basic-info-form .edit-button, #basic-info-form .add-button").click(function(){
        $(this).parent().parent().parent().addClass("edit-group-box-open").removeClass("not-open-group-box");
        $("#basic-info-form .edit-group-row .edit-group-box").addClass("not-open-group-box");

        $("#basic-info-form .cancel-button").click(function(){
            $(this).parent().parent().parent().removeClass("edit-group-box-open").removeClass("not-open-group-box");
            $("#basic-info-form .edit-group-row .edit-group-box").removeClass("not-open-group-box");
        });

    });

    $("#about-me-form .edit-button, #about-me-form .add-button").click(function(){
        $(this).parent().parent().parent().addClass("edit-group-box-open").removeClass("not-open-group-box");
        $("#about-me-form .edit-group-row .edit-group-box").addClass("not-open-group-box");

        $("#about-me-form .cancel-button").click(function(){
            $(this).parent().parent().parent().removeClass("edit-group-box-open").removeClass("not-open-group-box");
            $("#about-me-form .edit-group-row .edit-group-box").removeClass("not-open-group-box");
        });
    });

    $("#social-account-id-form .edit-button, #social-account-id-form .add-button").click(function(){
        $(this).parent().parent().parent().addClass("edit-group-box-open").removeClass("not-open-group-box");
        $("#social-account-id-form .edit-group-row .edit-group-box").addClass("not-open-group-box");

        $("#social-account-id-form .cancel-button").click(function(){
            $(this).parent().parent().parent().removeClass("edit-group-box-open").removeClass("not-open-group-box");
            $("#social-account-id-form .edit-group-row .edit-group-box").removeClass("not-open-group-box");
        });
    });

    /* login-and-security page */

    $("#login-and-security-form .update-button, #login-and-security-form .add-button").click(function(){
        $(this).parent().parent().parent().addClass("edit-group-box-open").removeClass("not-open-group-box");
        $("#login-and-security-form .edit-group-row .edit-group-box").addClass("not-open-group-box");

        $("#login-and-security-form .cancel-button").click(function(){
            $(this).parent().parent().parent().removeClass("edit-group-box-open").removeClass("not-open-group-box");
            $("#login-and-security-form .edit-group-row .edit-group-box").removeClass("not-open-group-box");
        });
    });

    /* End of login-and-security page */

    /* End of Add & edit form */

    /* supper-rating */

    var valueHover = 0;
    function calcSliderPos(e,maxV) {
        return (e.offsetX / e.target.clientWidth) *  parseInt(maxV,10);
    }
        
    $(".starrate").on("click",function(){
        $(this).data('val',valueHover);
        $(this).addClass('saved')
    });
        
    $(".starrate").on("mouseout",function(){
        upStars($(this).data('val'));
    });	

        
    $(".starrate span.ctrl").on("mousemove",function(e) { 
        var maxV = parseInt($(this).parent("div").data('max'))
        valueHover = Math.ceil(calcSliderPos(e,maxV)*2)/2;
        upStars(valueHover);
    });

    function upStars(val) {
        
        var val = parseFloat(val);
        $("#star-number-view").html( val.toFixed(1) );
        
        var full = Number.isInteger(val);
        val = parseInt(val);
        var stars = $("#starrate i");
        
        stars.slice(0,val).attr("class" , "fas fa-fw fa-star" );
        if(!full)  { stars.slice(val,val+1).attr("class" , "fas fa-fw fa-star-half-alt" ); val++ }
        stars.slice(val,5).attr("class" , "far fa-fw fa-star" );
        
        
    }	
                    
    
    $(".starrate span.ctrl").width($(".starrate span.cont").width());
    $(".starrate span.ctrl").height($(".starrate span.cont").height());


    /* End of supper-rating */


    // Input file
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    

    
});