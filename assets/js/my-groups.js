$(document).ready(function(){ 

    $(function () {
        
        var filterList = {
        
            init: function () {
            
                $('#manage-group-root').mixItUp({
                selectors: {
                    target: '.group-card-col',
                    filter: '.filter',
                },
                load: {
                filter: '.own-group-filter, .admin-group-filter, .member-group-filter'  
            }     
                });								
            
            }

        };
        // Run the show!
        filterList.init();
        
        
    });	

    /* profile pic */
    
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".file-upload").on('change', function(){ readURL(this); });
    $(".upload-button").on('click', function() { $(".file-upload").click(); });
    
    /* end of pic */

     /* select 2 - multiple */

     var CustomSelectionAdapter = $.fn.select2.amd.require("select2/selection/customSelectionAdapter");
  
    $('#keywords-select').select2({ })

    $('#keywords-select').select2({
        tags: false,
        selectionAdapter: CustomSelectionAdapter,
        placeholder: "Enter keywords",
        allowClear: true
    });
     
    $('#keywords-select + .select2').click(function() {    
        $(".select2-container").addClass("active-select2-multiple-dropdown");        
    });

    $('#search-cuisines').select2({ })
    $('#search-cuisines').select2({
        tags: false,
        selectionAdapter: CustomSelectionAdapter,
        placeholder: "Search for specific cuisines",
        allowClear: true
    });
     
    $('#search-cuisines + .select2').click(function() {    
        $(".select2-container").addClass("active-select2-multiple-dropdown");        
    });

    $('#search-invite-member').select2({ })
    $('#search-invite-member').select2({
        tags: false,
        selectionAdapter: CustomSelectionAdapter,
        placeholder: "Find members",
        allowClear: true
    });
     
    $('#search-invite-member + .select2').click(function() {    
        $(".select2-container").addClass("active-select2-multiple-dropdown");        
    });
    
    /* End of select 2 */

    /* Read more */

    var showChar = 420;  
    var ellipsestext = "...";
    var moretext = "Read more";
    var lesstext = "Read less";
    

    $('#read-more-desc1').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });

    /* select checkbox */

    /**  Invite Friends total count in number  **/
    var $checkboxes = $('.custom-control-input[name=invite-friends-option-top01][type="checkbox"]');
    $checkboxes.change(function(){
        var countCheckedCheckboxes = $checkboxes.filter(':checked').length;
        $('#send-invitations1 .number-selected').text(countCheckedCheckboxes);
    });

    /** End of Invite Friends total count in number  **/

    /** select all click check box **/
    var state = false; 
    
    $('#invite-all-select-btn').click(function () {
        $('.custom-control-input[name=invite-friends-option-top01]').each(function() {
            if(!state) {
                this.checked = true;
                var countCheckedCheckboxes = $checkboxes.filter(':checked').length;
                $('#send-invitations1 .number-selected').text(countCheckedCheckboxes);
            } else {
                this.checked = false;
                var countCheckedCheckboxes = $checkboxes.filter(':checked').length;
                $('#send-invitations1 .number-selected').text(countCheckedCheckboxes);
            }
        });
        
        if (state) {
            state = false;   
        } else {
            state = true;
        }
    });

    /* End of select checkbox */

});	
