/* select 2 */
$(".js-select2").select2({ minimumResultsForSearch: -1 });

$("#listing_cuisine_id").select2({
  placeholder: "Choose cuisines",
  allowClear: true
});
$("#listing_course_id").select2({
  placeholder: "Choose course",
  allowClear: true
});
/* End of select 2 */

/* datepicker get */
// $('#date-picker01').datepicker({ showOtherMonths: true });

var date = new Date();
var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
$('#date-picker01').datepicker({ 
  minDate: today
});
/* End of datepicker get */

/* number limited */
function maxLengthCheck(object) {
    if (object.value.length > object.maxLength)
      object.value = object.value.slice(0, object.maxLength)
  }
    
  function limite_listing_price (evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode (key);
    var regex = /[0-9]|\./;
    if ( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }

  function total_servings_price (evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode (key);
    var regex = /[0-9]|\./;
    if ( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }
  
  

/* End of number limited */

/*
$(".category-error1").html("This field is required.");
$(".step-btn.btn-next").click(function() {
  if(!$(".option-group[name = 'category'] .select.option").hasClass("selected")){
    $(".category-error1").addClass("d-inline-block").html("This field is required.").removeClass("d-none").css("display", "none");
  } 
  else{
    $(".category-error1").removeClass("d-inline-block").html("This field is required.").addClass("d-none").css("display", "block");
  }
});

$(".category-error2").html("This field is required.");
$(".step-btn.btn-next").click(function() {
  if(!$(".option-group[name = 'listing_shape'] .select.option").hasClass("selected2")){
    $(".category-error2").addClass("d-inline-block").html("This field is required.").removeClass("d-none").css("display", "none");
    $("#validation-for-choose-type").addClass("d-none");
  } 
  else{
    $("#validation-for-choose-type").addClass("d-none");
    $(".category-error2").removeClass("d-inline-block").html("This field is required.").addClass("d-none").css("display", "block");
  }
});

$(".option-group[name = 'listing_shape'] .select.option").on("click", function() {
  var selectedItem = $(this);
  $(".option-group[name = 'listing_shape'] .select.option").each(function(index, val) {
    $(this).removeClass('selected');
  });
  selectedItem.addClass('selected');
});
*/