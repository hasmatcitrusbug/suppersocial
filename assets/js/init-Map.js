let allMarkers = [];
function initMap() { 
    var uluru = {lat: -33.028249, lng: 150.274856};
    var map = new google.maps.Map(document.getElementById('map-canvas'), {
      zoom: 8,
      center: uluru,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var contentString = '<div class="image-grid-card image-round-grid-card">' +
    '<div class="img-banner-grid-top">' +

        '<div class="slider-div">' +

            '<div class="owl-carousel owl-theme owl-slider-card" id="map-owl-slider-card">' +
        
            '<div class="item">' +
                '<div class="custom-img-card">' +
                '<img src="assets/images/food/chicken-stuff.jpg" class="img-fluid img-slider" alt="img" />' +
                '</div>' +
            '</div>' +
            '<div class="item">' +
                '<div class="custom-img-card">' +
                '<img src="assets/images/food/redmeat.jpg" class="img-fluid img-slider" alt="img" />' +
                '</div>' +
            '</div>' +
            '<div class="item">' +
                '<div class="custom-img-card">' +
                '<img src="assets/images/share-a-meal/carbs-cover.jpg" class="img-fluid img-slider" alt="img" />' +
                '</div>' +
            '</div>' +

            '</div>' +

            '<div class="top-right-div">' +
            '<a href="#" class="btn btn-round-circle btn-social"><i class="share-icon"></i></a>' +
            '<button class="btn btn-round-circle btn-like like-round-btn" id="like-btn1">' +
                '<i class="like-icons like-icon"></i>' +
                '<i class="like-icons like-fill-icon"></i>' +
            '</button>' +
            '</div>' +

        '</div>' +

        '</div>' +
        '<div class="image-grid-body">' +
        '<div class="content-div">' +

            '<div class="grid-content-top-div">' +
            '<p class="color-p"><span class="span-title">Meals Offered</span> <span class="span-bold">Pork, Dairy</span></p>' +
            '<div class="badge-div">' +
              '<img src="assets/images/badge-icons/hobby-chef-badge.svg" alt="Hobby chef" class="img-fluid img-badge" data-toggle="tooltip" data-placement="top" html="true" title="<p>Hobby chef</p>" />'+
              '<img src="assets/images/badge-icons/foodie-icon.svg" alt="Foodie" class="img-fluid img-badge" data-toggle="tooltip" data-placement="top" html="true" title="<p>Foodie</p>" />'+
              '<img src="assets/images/badge-icons/perfect-badge.svg" alt="Perfect" class="img-fluid img-badge" data-toggle="tooltip" data-placement="top" html="true" title="<p>Perfect</p>" />'+
            '</div>' +
            '</div>' +

            '<div class="grid-content-middle-div">' +
            '<a href="#"><h3>Specialist in chicken stuff</h3></a>' +
            '<p class="prize-text"> <span class="text-number">500</span> <span class="text-service">/ Serving</span> </p>' +
            '</div>' +

        '</div>' +
        '<div class="grid-content-bottom-div">' +
            '<div class="left-bottom-div">' +
            '<div class="img-thumb d-flex"><img src="assets/images/popular1.jpg" class="img-fluid img-user" alt="user" /></div>' +
            '<div class="content-user">' +
                '<a href="" class="user-profile-link"><h4>Piter Minter</h4></a>' +
                '<p>Feb 10 2020</p>' +
            '</div>' +
            '</div>' +
            '<div class="right-bottom-div">' +
            '<a href="#" class="btn btn-view-more-link">View more</a>' +
            '</div>' +
        '</div>' +
    '</div>' +
'</div>';

    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });

    var icon1 = "http://13.235.236.166/HTML/suppersocial/assets/images/icons/map-pin/map-pin.svg";
    var icon2 = "http://13.235.236.166/HTML/suppersocial/assets/images/icons/map-pin/map-pin-hover.svg";
    var marker = "";
    

    var locations = [
        ['Branch1', -33.028249, 151.244856, 1],
        ['Branch2', -32.923036, 150.259052, 2],
        ['Branch3', -32.80576, 149.970872, 3],
        ['Branch4', -33.800101, 150.000000, 4],
        ['Branch5', -33.953228, 151.247040, 5],
        ['Branch6', -32.450198, 150.294302, 6],
        ['Branch7', -33.817958, 150.0138238, 7],
        ['Branch8', -32.250198, 150.218302, 8],

      ];

    for (i = 0; i < locations.length; i++) {  
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(locations[i][1], locations[i][2]),
          map: map,
          id: 'marker'+i,
          icon: icon1,
        });

        allMarkers.push(marker);

        // marker.addListener('click', function() {
        //   infowindow.open(map, marker);
        //   $('#map-owl-slider-card').owlCarousel({
        //     loop: true, nav: true,
        //     navText: ['<span class="span-roundcircle left-roundcircle"><span class="material-icons">chevron_left </span>', '<span class="span-roundcircle right-roundcircle"><span class="material-icons">chevron_right </span></span>'], items: 1,
        //     singleItem: true
        //   });
        //   $('#like-btn1').on('click', function(){
        //       $(this).toggleClass("active");
        //   });
        // });

        // google.maps.event.addListener(infowindow, 'domready', function (event) {
        //   $('#map-owl-slider-card').owlCarousel({
        //   loop: true, nav: true,
        //     navText: ['<span class="span-roundcircle left-roundcircle"><span class="material-icons">chevron_left </span>', '<span class="span-roundcircle right-roundcircle"><span class="material-icons">chevron_right </span></span>'], items: 1,
        //     singleItem: true 
        //   });
        //   $('#like-btn1').on('click', function(){
        //     $(this).toggleClass("active");
        //   });
        // });
        google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {        
          return function() {
            marker.setIcon(icon2);
          }
        })(marker, i));
        google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {        
          return function() {
            marker.setIcon(icon1);
          }
        })(marker, i));
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          
          $('#map-owl-slider-card').owlCarousel({
            loop: true,
            nav: true,
            navText: ['<span class="span-roundcircle left-roundcircle"><span class="material-icons">chevron_left </span>', '<span class="span-roundcircle right-roundcircle"><span class="material-icons">chevron_right </span></span>'],
            items: 1,
            singleItem: true,
          });
          $('#like-btn1').on('click', function(){
            $(this).toggleClass("active");
          });
          return function() {
            infowindow.setContent(contentString);
            infowindow.open(map, marker);
          }
        })(marker, i));
        google.maps.event.addListener(infowindow, 'domready', function (event) {
          $('#map-owl-slider-card').owlCarousel({ loop: true, nav: true,
              navText: ['<span class="span-roundcircle left-roundcircle"><span class="material-icons">chevron_left </span>', '<span class="span-roundcircle right-roundcircle"><span class="material-icons">chevron_right </span></span>'], items: 1,
              singleItem: true })
              $('#like-btn1').on('click', function(){
                $(this).toggleClass("active");
              });
              // jQuery('[data-toggle="tooltip"]').tooltip({placement:'bottom'});
              $('[data-toggle="tooltip"]').tooltip({html: true, animation: true, trigger: "hover" });
        });
      }
  }

  var icon1 = "http://13.235.236.166/HTML/suppersocial/assets/images/icons/map-pin/map-pin.png";
  var icon2 = "http://13.235.236.166/HTML/suppersocial/assets/images/icons/map-pin/map-pin-hover.png";
  $(".marker-hover-effect").mouseover(function(){
    var curretnId = $(this).data('custom-id');
    for ( var i = 0; i< allMarkers.length; i++) {
      if (curretnId === allMarkers[i].id) {
        allMarkers[i].setIcon(icon2);
        break;
      }
    }
  });
  $(".marker-hover-effect").mouseout(function(){
    var curretnId = $(this).data('custom-id');
    for ( var i = 0; i< allMarkers.length; i++) {
      if (curretnId === allMarkers[i].id) {
        allMarkers[i].setIcon(icon1);
        break;
      }
    }
  });
  // function hover(id) {
  //   for ( var i = 0; i< allMarkers.length; i++) {
  //       if (id === allMarkers[i].id) {
  //         allMarkers[i].setIcon(icon1);
  //         break;
  //       }
  // }
  // }

  // //Function called when out the div
  // function out(id) {  
  //   for ( var i = 0; i< allMarkers.length; i++) {
  //       if (id === allMarkers[i].id) {
  //         allMarkers[i].setIcon(icon2);
  //         break;
  //       }
  // }
  // }