/* Set navigation */

function openNav() {
  $(".Topbar__topbarMobileMenu__3z-JE").addClass("MenuMobile__canvasOpen__3CorC");
  $("body").addClass("MenuMobile__menuMobileOpen__3OIsv");
}

function closeNav() {
  $(".Topbar__topbarMobileMenu__3z-JE").removeClass("MenuMobile__canvasOpen__3CorC");
  $("body").removeClass("MenuMobile__menuMobileOpen__3OIsv");
} 

function open_searchbox(){
  $("body").toggleClass("SearchBar__mobileSearchOpen__34aWg");
  $(".SearchBar__form__27PQI").toggleClass("open-menu");
  
}


$(document).ready(function(){ 

  $(".MenuMobile__menuLabelMobile__3uXwZ").click(function(){
    openNav(); 
  });

  $(".MenuMobile_overlay").click(function(){
    closeNav(); 
  });

  $(".SearchBar__mobileToggle__3pjye").click(function(){
    open_searchbox(); 
  });

  $(".AvatarDropdown__avatarWithNotifications__1TdZT").click(function(){
    $(".AvatarDropdown").toggleClass('AvatarDropdown__openDropdown__1PYh9');
  });

  $(window).scroll(function() {
    if ($(this).scrollTop() >= 150) {        
        $('.return-to-top').addClass("display_show");    
    } else {
        $('.return-to-top').removeClass("display_show");   
    }
  });

  $('.return-to-top').click(function() {    
    $('body,html').animate({
        scrollTop : 0,                 
    }, 10);
  });

  $(window).scroll(function(){
    var sticky = $('.header-div'),
        scroll = $(window).scrollTop();
  
      if (scroll >= 140) sticky.addClass('header-bgcolor');
      else sticky.removeClass('header-bgcolor');
  
  });

  if ($('body').width() < 1025 ){
    $(window).scroll(function(){
      var sticky = $('.header-div'),
          scroll = $(window).scrollTop();
    
        if (scroll >= 80) sticky.addClass('header-bgcolor');
        else sticky.removeClass('header-bgcolor');
    
    });
  }
 
});


/* end of navigation */