$(document).ready(function() {

    /* read more */
    var showChar = 280;  
    var ellipsestext = "...";
    var moretext = "Read more";
    var lesstext = "Read less";

    $('#my-reviews-rating #read-more-description1').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });

    /* End of read more */

    // $(window).scroll(function(){
    //     var sticky = $('#my-rating-fixed-box'),
    //     scroll = $(window).scrollTop();
    
    //     if (scroll >= 256) sticky.addClass('white-box-rating-fixed');
    //     else sticky.removeClass('white-box-rating-fixed');
    
    // });

});