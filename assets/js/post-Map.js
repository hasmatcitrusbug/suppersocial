var map;
function initMap() {
  map = new google.maps.Map(
      document.getElementById('map-post-canvas'),
      {center: new google.maps.LatLng(-33.91722, 151.23064), zoom: 14});

  var iconBase =
      'http://13.235.236.166/HTML/suppersocial/assets/images/icons/';

  var icons = {
    info: {
      icon: iconBase + 'pin-custom1.png'
    }
  };

  var features = [
    {
      position: new google.maps.LatLng(-33.91721, 151.22630),
      type: 'info'
    }
  ];

  // Create markers.
  for (var i = 0; i < features.length; i++) {
    var marker = new google.maps.Marker({
      position: features[i].position,
      icon: icons[features[i].type].icon,
      map: map
    });
  };
}