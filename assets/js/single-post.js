$(window).load(function() {
    $("#loader").delay(1000).fadeOut("slow");
    $("#over-layer").delay(1000).fadeOut("slow");
})

 /* select 2 */
$(".js-select2").select2({ minimumResultsForSearch: -1 });

$("#trade-type-select").select2({
    placeholder: "Select trade type"
});
$('#trade-type-select + .select2').click(function() {    
    $(".select2-container").addClass("active-select2-dropdown");        
});

$("#trade-method-select").select2({
    placeholder: "Select trade method"
});
$('#trade-method-select + .select2').click(function() {    
    $(".select2-container").addClass("active-select2-dropdown");        
});

/* end fo select 2 */

$('.sp-banner-top-right #btn-save-item').on('click', function(){
    $(this).toggleClass("active");
});

/* datepicker get */

var date = new Date();
var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
$('#date-picker-inline').datetimepicker({ 
  minDate: today,
  inline: true,
  sideBySide: true,
});
// clearBtn: true
$("#reset-date").click(function(){
    var sysdate = new Date();
    $('#date-picker-inline').data("DateTimePicker").date(new Date())
})
/* End of datepicker get */

$(document).ready(function() {

    /* read more */
    var showChar = 250;  
    var ellipsestext = "...";
    var moretext = "Read more";
    var lesstext = "Read less";

    $('#read-more-description1').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });

    $('.read-more-comment-content').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });

    /* End of read more */

    $(window).scroll(function(){
    var sticky = $('.right-post-area'),
        scroll = $(window).scrollTop();
    
        if (scroll >= 640) sticky.addClass('fixed-right-post-area');
        else sticky.removeClass('fixed-right-post-area');
    
    });

    /*** Lightbox *****/

    $("#lightgallery1").lightGallery({
        thumbnail:true,
        download:false,
        progressBar:true,
        zoom:false,
        pager:false,
        fullScreen:false,
        share:false,
        addClass: 'fixed-size-container',
        thumbWidth:215,
        thumbMargin:15
      });
  
      $('#open-show-photos-light-box').on('click', function() {
        $("#lightgallery1").lightGallery({  });
        $("#lightgallery1>div").click();
      });
  
      var numItems = $('.images-list-row .img-col-banner').length;
      $(".banner-div").addClass("banner-thumb-" + numItems);
  

    /*** End of Lightbox *****/

});