$(document).ready(function() {

     /* select 2 */
     $(".js-select2").select2({ minimumResultsForSearch: -1 });
    
     $("#trade-offers-select").select2({
         placeholder: "All messages"
     });
     
     $('#trade-offers-select + .select2').click(function() {    
         $(".select2-container").addClass("active-select2-dropdown");        
     });

     $("#trade-requests-select").select2({
        placeholder: "All messages"
    });
    
    $('#trade-requests-select + .select2').click(function() {    
        $(".select2-container").addClass("active-select2-dropdown");        
    });

     
     /* End of select 2 */

    /* read more */
    var showChar = 240;  
    var ellipsestext = "...";
    var moretext = "Read more";
    var lesstext = "Read less";

    $('#trade-offers-messages #read-more-desc-1').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });

    $('#trade-requests-messages #read-more-desc-1').each(function() {
        var content = $(this).html();
        var showChar = 160;
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });

    $('#friends-requests-messages #read-more-desc-1').each(function() {
        var content = $(this).html();
        var showChar = 160;
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });

    $('#group-messages #read-more-desc-1').each(function() {
        var content = $(this).html();
        var showChar = 160;
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });

    
    
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });

    /* End of read more */

});