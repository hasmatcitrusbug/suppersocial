$(document).ready(function(){ 
    $('#owl-supper-stars').owlCarousel({
        loop:true,
        nav:false,
        center:true,
        dots: false,
        autoWidth: false,
        stagePadding: 0,
        margin:0,
        smartSpeed:1500,
        responsive:{
            0:{
                items:1 
            },
            768:{
                items:3
            },
            1180:{
                items:4
            },
            1400:{
                items:4
            },
            1600:{
                items:5
            } 
        }
      });

      $('#owl-post-img-view').owlCarousel({
        loop:false,
        nav:true,
        navText: ['<span class="span-roundcircle left-roundcircle"><span class="material-icons">chevron_left </span>','<span class="span-roundcircle right-roundcircle"><span class="material-icons">chevron_right </span></span>'],
        dots: false,
        autoWidth: false,
        stagePadding: 0,
        margin:0,
        smartSpeed:1500,
        responsive:{
            0:{
                items:1 
            },
            600:{
                items:2
            },
            1025:{
                items:3
            },
            1600:{
                items:4
            } 
        }
      });
});