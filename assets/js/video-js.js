
    /* video */

    var video1 = document.getElementById("Introduction-video");
    var btn1 = document.getElementById("video-button");
    
    video1.pause();

    
    function videoFunction() {
        if (video1.paused) {
            video1.play();
            btn1.innerHTML = "<span class='video-icon-span pause-video-icon'> <span class='material-icons'> pause </span> </span>";
        } else {
            video1.pause();
            btn1.innerHTML = "<span class='video-icon-span play-video-icon'> <span class='material-icons'> play_arrow </span> </span> ";
        }
    }

    /* End of video */    