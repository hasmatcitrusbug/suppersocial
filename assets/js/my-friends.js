$(document).ready(function(){
    
    /* select 2 */
    $(".js-select2").select2({ minimumResultsForSearch: -1 });
    
    $("#results-per-page-select").select2({
        placeholder: "Results Per Page"
    });
    
    $('#results-per-page-select + .select2').click(function() {    
        $(".select2-container").addClass("active-select2-dropdown");        
    });
    /* End of select 2 */

    // $('.btn-un-follow').click(function() {    
    //     $(this).addClass("btn-follow").removeClass("btn-un-follow").text("Follow");        
    // });

    $('.btn-follow').click(function() {    
        $(this).toggleClass("btn-un-follow");        
        $(this).text(function(i, text){
            return text === "Follow" ? "Unfollow" : "Follow";
        })
    });

    $('.btn-un-follow').click(function() {    
        $(this).toggleClass("btn-follow");        
        $(this).text(function(i, text){
            return text === "Unfollow" ? "Follow" : "Unfollow";
        })
    });


    /* select checkbox */

    /**  Invite Friends total count in number  **/
    var $checkboxes = $('.custom-control-input[name=invite-friends-option-top01][type="checkbox"]');
    $checkboxes.change(function(){
        var countCheckedCheckboxes = $checkboxes.filter(':checked').length;
        $('#send-invitations1 .number-selected').text(countCheckedCheckboxes);
    });

    /** End of Invite Friends total count in number  **/

    /** select all click check box **/
    var state = false; 
    
    $('#invite-all-select-btn').click(function () {
        $('.custom-control-input[name=invite-friends-option-top01]').each(function() {
            if(!state) {
                this.checked = true;
                var countCheckedCheckboxes = $checkboxes.filter(':checked').length;
                $('#send-invitations1 .number-selected').text(countCheckedCheckboxes);
            } else {
                this.checked = false;
                var countCheckedCheckboxes = $checkboxes.filter(':checked').length;
                $('#send-invitations1 .number-selected').text(countCheckedCheckboxes);
            }
        });
        
        if (state) {
            state = false;   
        } else {
            state = true;
        }
    });

    /* End of select checkbox */

    /**  For Find my friend  */

     /* select checkbox */

    /**  Invite Friends total count in number  **/
    var $checkboxes2 = $('.custom-control-input[name=find-my-friend-option-top01][type="checkbox"]');
    $checkboxes2.change(function(){
        var countCheckedCheckboxes = $checkboxes2.filter(':checked').length;
        $('#send-friend-request1 .number-selected').text(countCheckedCheckboxes);
    });

    /** End of Invite Friends total count in number  **/

    /** select all click check box **/
    var state2 = false; 
    
    $('#select-all-select-btn').click(function () {
        $('.custom-control-input[name=find-my-friend-option-top01]').each(function() {
            if(!state) {
                this.checked = true;
                var countCheckedCheckboxes = $checkboxes2.filter(':checked').length;
                $('#send-friend-request1 .number-selected').text(countCheckedCheckboxes);
            } else {
                this.checked = false;
                var countCheckedCheckboxes = $checkboxes2.filter(':checked').length;
                $('#send-friend-request1 .number-selected').text(countCheckedCheckboxes);
            }
        });
        
        if (state) {
            state = false;   
        } else {
            state = true;
        }
    });

    /* End of select checkbox */

});