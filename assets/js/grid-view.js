$(document).ready(function(){
    
    /* select 2 */
    $(".js-select2").select2({ minimumResultsForSearch: -1 });
    
    $("#results-per-page-select").select2({
        placeholder: "Results Per Page"
    });
    $("#sort-by-select").select2({
        placeholder: "Sort By"
    });
    
    $('#results-per-page-select + .select2').click(function() {    
        $(".select2-container").addClass("active-select2-dropdown");        
    });

    $('#sort-by-select + .select2').click(function() {    
        $(".select2-container").addClass("active-select2-dropdown");        
    });

    /* select 2 - multiple */

    var CustomSelectionAdapter = $.fn.select2.amd.require("select2/selection/customSelectionAdapter");
  
    $('#Cuisine-multiple').select2({ })
   
    $('#Cuisine-multiple').select2({
        tags: false,
        selectionAdapter: CustomSelectionAdapter,
        placeholder: "Choose cuisines",
        allowClear: true
    });

    $('#Course-multiple').select2({ })
   
    $('#Course-multiple').select2({
        tags: false,
        selectionAdapter: CustomSelectionAdapter,
        placeholder: "Choose courses",
        allowClear: true
    });

    /* End of select 2 */

    
    /* dropdown1 */

    $('div.dropdown-menu.dropdown-menu-custom').on('click', function(event){
        event.stopPropagation();
    });
    
    $('#trade-type-save1').on('click', function(){
        var test = new Array();
        if ($("input.custom-control-input[name='Trade-Type']:checked").is(':checked')) {
          $("input.custom-control-input[name='Trade-Type']:checked").each(function() {
              test.push($(this).val());
          });
          $("#trade-type-btn-filter1").text(test);
          $('#dropdown-menu-custom1, #overlay1').removeClass('show');
        }
        else{
          $("#trade-type-btn-filter1").text("Trade Type");
          $('#dropdown-menu-custom1, #overlay1').removeClass('show');
        }
    });

    /* End of dropdown1 */

    /* dropdown2 */

    $('#trade-type-save2').on('click', function(){
        $("#trade-type-btn-filter2").text("Cuisine");
    });

    $('.top-right-div #like-btn').on('click', function(){
        $(this).toggleClass("active");
    });

    /* End of dropdown1 */

    /* slider owl */

    
    $('.owl-slider-card').owlCarousel({
        loop:true,
        nav:true,
        mouseDrag: false,
        navText: ['<span class="span-roundcircle left-roundcircle"><span class="material-icons">chevron_left </span>','<span class="span-roundcircle right-roundcircle"><span class="material-icons">chevron_right </span></span>'],
        dots: true,
        stagePadding: 0,
        margin:0,
        smartSpeed:1000,
        items: 1,
        singleItem: true
    });

    /* End of slider */

    // $('#map-owl-slider-card').owlCarousel({
    //     loop:true,
    //     nav:true,
    //     mouseDrag: false,
    //     navText: ['<span class="span-roundcircle left-roundcircle"><span class="material-icons">chevron_left </span>','<span class="span-roundcircle right-roundcircle"><span class="material-icons">chevron_right </span></span>'],
    //     dots: true,
    //     stagePadding: 0,
    //     margin:0,
    //     smartSpeed:1000,
    //     items: 1,
    //     singleItem: true,
    // });
    
    
    $('.owl-slider-card').trigger('refresh.owl.carousel');

    $('#show-map-button').on('click', function(){
        $(".grid-view-section").addClass("list-view-section");
        $(".grid-view-header-top").addClass("list-view-header-top");
        $(".grid-view-filter-top").addClass("list-view-filter-top");
        $(".grid-view-body").addClass("list-view-body");
        $(".map-view-section.display-none").removeClass("display-none");
        $('.owl-slider-card').trigger('refresh.owl.carousel');
    });

    $('#close-map').on('click', function(){
        $(".grid-view-section").removeClass("list-view-section");
        $(".grid-view-header-top").removeClass("list-view-header-top");
        $(".grid-view-filter-top").removeClass("list-view-filter-top");
        $(".grid-view-body").removeClass("list-view-body");
        $(".map-view-section").addClass("display-none");
        $('.owl-slider-card').trigger('refresh.owl.carousel');
    });

    if ($('body').width() < 1023 ){
        $('#show-map-button').on('click', function(){
            $(".grid-view-section").removeClass("list-view-section");
            $(".grid-view-header-top").removeClass("list-view-header-top");
            $(".grid-view-filter-top").removeClass("list-view-filter-top");
            $(".grid-view-body").removeClass("list-view-body");
            $('.owl-slider-card').trigger('refresh.owl.carousel');
        });
    }


    /* loader */

    $('[data-toggle="tooltip"]').tooltip({html: true, animation: true});   
    
    
});
